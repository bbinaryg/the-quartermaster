import { Message, MessageEmbed } from "discord.js";
// import { client } from "Bot";
import { prefix } from "Config";
import { MyCommand } from "@client";
// import { emoji } from "@functions/FindUserEmoji";

export default class CommandName extends MyCommand {
    public constructor() {
        super("Event", {
            aliases: ["event","events"],
            category:"Utility",
            channel: "guild",
            description: {
                content:"Super long and detailed long description",
                snippet:"Creates an event.",
                usage: "what is it used for",
                examples: [
                    `${prefix}event`
                ],
            },
            ratelimit: 1  // number of times you can spam this sucker

        });
    }

    public  exec(msg: Message): Promise<void|Message> {

        const user = msg.author;

        // Enter the event title
        //Up to 200 characters are permitted
        const eventTitleEmbed = new MessageEmbed()
            .setColor("RANDOM")
            .setTitle("Enter the event title")
            .setDescription("Up to 200 characters are permitted");
        user.send(eventTitleEmbed).then(() => {
            const filter = m => msg.author.id === m.author.id;

            msg.channel.awaitMessages(filter, { time: 60000, max: 1, errors: ["time"] })
                .then(messages => {
                    msg.channel.send(`You've entered: ${messages.first().content}`);
                })
                .catch(() => {
                    msg.channel.send("You did not enter any input!");
                });
        });

        //Enter the event description
        // Type None for no description. Up to 1600 characters are permitted.
        const eventDescriptionEmbed = new MessageEmbed()
            .setColor("RANDOM")
            .setTitle("Enter the event description")
            .setDescription("Type `None` for no description. Up to 1600 characters are permitted.");
        user.send(eventDescriptionEmbed);

        // Signup options
        //      By default, the signup options are Accepted, Declined, or Tentative.
        //          1 Keep the defaults
        //          2 Configure signup options
        //      Enter the number of one of the above options

        const eventSignupEmbed = new MessageEmbed()
            .setColor("RANDOM")
            .setTitle("Signup options")
            .setDescription("By default, the signup options are **Accepted**, **Declined**, or **Tentative**.\n\n**1** Keep the defaults\n**2** Configure signup options")
            .setFooter("Enter the number of one of the above options");
        user.send(eventSignupEmbed);

        // Enter the maximum number of attendees
        // Type None for no limit. Up to 100 attendees are permitted.

        // Enter your timeZone


        // When should the event start?
        //     Friday at 9pm
        //     Tomorrow at 18:00
        //     Now
        //     In 1 hour
        //     YYYY-MM-DD 7:00 PM

        // What is the duration of this event?
        // Type None for no duration.
        //      2 hours
        //      45 minutes
        //      1 hour and 30 minutes

        // How often should this event repeat?
        //     1 Never
        //     2 Daily
        //     3 Weeklys
        //     4 Weekdays (Monday to Friday)
        //     5 Monthly on the 14th
        //     6 Custom
        // Enter a number to select an option
        // To exit, type 'cancel'

        // Advanced options
        // Don't need anything fancy?
        //     1 Finish event creation
        // Or customize the event:
        //     2 Mention roles on event creation
        //     3 Add an image
        //     4 Change the event color
        //     5 Restrict signup to certain roles
        //     6 Select multiple signup options
        //     7 Add Title URL
        // Enter a number to select an option

        //------------------
        // Event mentions

        // 1 Continue with the above event mentions
        // 2 Add an event mention
        // 3 Remove an event mention
        // Enter a number to select an option

        return;
    }
}
