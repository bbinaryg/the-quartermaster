import { Message } from "discord.js";
import { prefix } from "Config";
import { MyCommand } from "@client";
import axios from "axios";
import jssoup from "jssoup";
import { stripIndents } from "common-tags";

export default class CommandName extends MyCommand {
    public constructor() {
        super("ViciousMockery", {
            aliases: ["viciousmockery", "mock", "insult"],
            category:"Fun Commands",
            channel: "guild",
            description: {
                content:"Quartermaster goes and asks a lot of sailors in seedy taverns for insults and gives you his favorite to use.",
                snippet:"Receive aid from the Quartermaster in mocking a user.",
                usage: `${prefix}mock [USER]`,
                examples: [
                    `${prefix}viciousMockery @The Quartermaster` // !mock @Furious#0087
                ]
            },
            ratelimit: 3
        });
    }

    public exec( msg : Message): void /* Promise < void | Message > */ {

        let insult : string;
        const htmlEntities = {
            "&amp;"     : "&",
            "&lt;"      : "<",
            "&gt;"      : ">",
            "&quot;"    : "\"",
            "&apos;"    : "'"
        };

        axios.create()
            .get("https://www.kassoon.com/dnd/vicious-mockery-insult-generator/")
            .then((response) => {
                const soup = new jssoup(response.data);
                insult = (soup.findAll("p")[1].text as string).trim().replace(/&(amp|lt|gt|quot|apos);/g, match => htmlEntities[match]);

                msg.delete();
                if (msg.mentions.users.size) {
                    msg.channel.send(stripIndents`
                    <@${msg.mentions.users.first().id}>, ${insult.slice(0, 2).search(/I( |')/) !== -1 ? insult : insult.slice(0, 1).toLowerCase() + insult.slice(1)}
                    - ${msg.author}`
                    );
                } else {
                    msg.channel.send(`Something went wrong do \`${prefix}help viciousMockery\` to see how to use this command..`);
                    return;
                }
            })
            .catch(console.error);

        return;
    }
}
