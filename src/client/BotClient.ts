import{ AkairoClient, CommandHandler, ListenerHandler, CommandOptions, Command } from "discord-akairo";
import { Message } from "discord.js";
import { join } from "path";
import { prefix, owners } from "@config";

declare module "discord-akairo"{
    interface AkairoClient {
        CommandHandler: CommandHandler;
        ListenerHandler:ListenerHandler;
    }
}
interface BotOptions {
    token?: string;
    owners?: string | string[];
}

export default class BotClient extends AkairoClient {
    public config: BotOptions;
    public ListenerHandler:ListenerHandler = new ListenerHandler(this, {
        directory: join(__dirname, "..", "events")
    });
    public CommandHandler: CommandHandler = new CommandHandler(this, {
        directory: join(__dirname, "..", "commands"),
        prefix: prefix,
        allowMention: true,
        handleEdits: true,
        commandUtil:true,
        commandUtilLifetime: 3e5,
        defaultCooldown: 1e4,
        argumentDefaults:{
            prompt: {
                modifyStart: (_: Message, str: string): string => `${str}\n\nType \`cancel\` to cancel the command...`,
                modifyRetry:(_: Message, str: string): string => `${str}\n\nType \`cancel\` to cancel the command...`,
                // breakout: true,
                timeout: "You took to long, the command has now been canceled.",
                ended: "You exceeded the maximum amount of tries, this command has now been canceled. ",
                cancel: "This command has been canceled...",
                retries: 3,
                time: 3e4
            }
        },
        ignorePermissions: owners
    });

    public constructor(config: BotOptions){
        super({
            ownerID: config.owners
        });
        this.config = config;
    }

    private async _init(): Promise<void> {
        this.CommandHandler.useListenerHandler(this.ListenerHandler);
        this.ListenerHandler.setEmitters({
            CommandHandler: this.CommandHandler,
            ListenerHandler: this.ListenerHandler,
            process
        });
        this.CommandHandler.loadAll();
        this.ListenerHandler.loadAll();
    }

    public async start(): Promise<string> {
        await this._init();
        return this.login(this.config.token);
    }
}

export interface MyCommandOptions extends CommandOptions {
    snippet?: string;
    // rolePermissions?: string[]; add
}

export class MyCommand extends Command {
    public snippet: string;

    public constructor(id: string, options?: MyCommandOptions) {
        super(id, options);
        if (options.snippet) this.snippet = options.snippet;
    }
}
