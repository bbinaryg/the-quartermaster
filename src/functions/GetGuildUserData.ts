import { Message, GuildMember, Role } from "discord.js";

export function getUserFromMessage(msg: Message): GuildMember | undefined {
    const memCache = msg.guild.members.cache;
    const guildMem = memCache.get(msg.member.id);
    return guildMem;
}

export function getUserColorFromMessage(msg: Message): string | undefined {
    const guildMem = getUserFromMessage(msg);
    if (guildMem) return guildMem.displayHexColor; // <--- THIS SUCKS
}

export function getAvatarFromMessage(msg: Message): string | undefined {
    const guildMem = getUserFromMessage(msg);
    if (guildMem) return guildMem.user.displayAvatarURL({ dynamic: true });
}

export function getRolesFromMessage(msg: Message): Role[] | undefined {
    const guildMem = getUserFromMessage(msg);
    if (guildMem) return guildMem.roles.cache.array();
}
