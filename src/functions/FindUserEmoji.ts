import { GuildEmoji, Client } from "discord.js";
export function emoji(bot: Client, id: string): GuildEmoji {
    return bot.emojis.cache.find(emoji => emoji.name === id);
}