import { Listener } from "discord-akairo";
import { GuildMember, MessageEmbed } from "discord.js";
import { SQL } from "Bot";
import { DB } from "@db";

export default class GuildMemberAddListener extends Listener {
    public constructor() {
        super("guildMemberAdd", {
            emitter: "client",
            event: "guildMemberAdd",
            category: "client",
        });
    }

    public exec(member: GuildMember): void {

        console.log("\n----------------------------------\n");

        let userId = DB.getUserRowId(member.id);
        let guildId = DB.getGuildRowId(member.guild.id);
        let userGuildId;
        if (!guildId) { // catch for if the guild somehow didn't exist
            DB.insertGuild(member.guild.id);
            guildId = DB.getGuildRowId(member.guild.id);
        }
        if (!newUserGreet()) {
            /// FIXME Send to specified channel
            console.log(`\n\nWell well well... look who came crawling back onboard... good to see ya, ${member.user.username}!\n`);
        }
        if (SQL.prepare("SELECT timeout_end FROM user_guilds WHERE user_id=? AND guild_id=?").get(userId, guildId) > Date.now()) {
            /// TODO This needs to be tested.
            const timeoutEndDate = SQL.prepare("SELECT timeout_end FROM user_guilds WHERE user_id=? AND guild_id=?").get(userId, guildId);
            const now = Date.now();
            const remTime = timeoutEndDate - now;
            let m = Math.floor(remTime/60000);
            let h = Math.floor(m/60);
            const d = Math.floor(h/24);

            h %= 24;
            m %= 60;

            h = (h < 10) ? 0 + h : h;
            m = (m < 10) ? 0 + m : m;

            const TimeOutEmbed = new MessageEmbed()
                .setColor("RED")
                .setTitle("Time left ...")
                .setDescription(`${d} days, ${h} hours, and ${m} minutes.`)
                .setThumbnail("https://media.giphy.com/media/MFIptzHH7Zh1tmQU3C/giphy.gif");

            member.send("We see that you're not currently welcome aboard. Do your time and we'll reconsider."+ TimeOutEmbed);
        }

        /**
         * Creates user entry in the `users` table if one did not exist.
         * Returns `true` if the user was greeted, else `false`.
         */
        function newUserGreet(): boolean {
            if (!userId) {
                DB.insertUser(member.id);
                userId = DB.getUserRowId(member.id);
            }
            userGuildId = DB.getUserGuildRowId(userId, guildId);
            if (!userGuildId) {
                /// TODO Send a message telling them to read and accept the rules
                /// FIXME Send to specified channel
                DB.insertUserGuild(userId, guildId);
                userGuildId = DB.getUserGuildRowId(userId, guildId);
                console.log(`\n\nAhoy there ${member.user.username}! Welcome aboard the ${member.guild.name}`);
                return true;
            }
            return false;
        }
    }
}


// timeout_end: DATETIME != NaN -> We see that you're not currently welcome aboard. Do your time and we'll reconsider. Time left ...

/*
bot.on('guildMemberAdd', member => {
    member.guild.channels.get('channelID').send('**' + member.user.username + '**, has joined the server!');
});
*/